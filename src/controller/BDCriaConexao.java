/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.sql.*;

/**
 *
 * @author nickv
 */

public class BDCriaConexao {
    
    public static Connection getConexao() throws SQLException {
        
        try{
            
            Connection conexao;
            System.out.println("Conectando ao banco de dados...");
            conexao = DriverManager.getConnection("jdbc:sqlite:remedios");
            System.out.println("Conectado ao BD!");
            return conexao; 

        }catch (SQLException e) {
            throw new SQLException(e.getMessage());
        }   
        
    }
    
}
