/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

/**
 *
 * @author nickv
 */
public class JSONGeral {

    private String id;
    private String ip;
    private String nome;
    private String porta;
    private String msg;
    private String nomeenv;
    private String nomereceb;
    private String acao;
    private String res;
    private String op;
    private String nomeremedio;
    private String equipe;
    private String quantidade;
    /**
     * @return the ip
     */
    
    public String getQuantidade (){
        return quantidade;
    }
    
    public void setQuantidade(String quantidade) {
        this.quantidade = quantidade;
    }
    
    public String getEquipe (){
        return equipe;
    }
    
    public void setEquipe(String equipe) {
        this.equipe = equipe;
    }
    
    public String getNomeRem (){
        return nomeremedio;
    }
    
    public void setNomeRem(String nomeremedio) {
        this.nomeremedio = nomeremedio;
    }
    
    public String getIp() {
        return ip;
    }

    /**
     * @param ip the ip to set
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    
    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the porta
     */
    public String getPorta() {
        return porta;
    }

    /**
     * @param porta the porta to set
     */
    public void setPorta(String porta) {
        this.porta = porta;
    }

    /**
     * @return the msg
     */
    public String getMsg() {
        return msg;
    }

    /**
     * @param msg the msg to set
     */
    public void setMsg(String msg) {
        this.msg = msg;
    }

    /**
     * @return the nomeenv
     */
    public String getNomeenv() {
        return nomeenv;
    }

    /**
     * @param nomeenv the nomeenv to set
     */
    public void setNomeenv(String nomeenv) {
        this.nomeenv = nomeenv;
    }

    /**
     * @return the nomereceb
     */
    public String getNomereceb() {
        return nomereceb;
    }

    /**
     * @param nomereceb the nomereceb to set
     */
    public void setNomereceb(String nomereceb) {
        this.nomereceb = nomereceb;
    }

    /**
     * @return the acao
     */
    public String getAcao() {
        return acao;
    }

    /**
     * @param acao the acao to set
     */
    public void setAcao(String acao) {
        this.acao = acao;
    }

    /**
     * @return the res
     */
    public String getRes() {
        return res;
    }

    /**
     * @param res the res to set
     */
    public void setRes(String res) {
        this.res = res;
    }

    public String getOp() {
        return op;
    }
    
     public void setOp(String op) {
        this.op = op;
    }
    
    
}
