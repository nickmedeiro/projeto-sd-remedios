package controller;

import View.*;
import com.google.gson.Gson;
import java.io.*;
import java.net.*;
import java.sql.*;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;


public class Conexao extends Thread {
    Socket clientSocket;
    public static ArrayList<Socket> listaSocket = new ArrayList<Socket>();
    public static ArrayList<PrintWriter> socketp = new ArrayList<PrintWriter>();
    public static ArrayList<Clientes> listaClientes = new ArrayList<Clientes>();
    public static ArrayList<String> clienteJson = new ArrayList<String>();
    public static List<JSONGeral> resul = new ArrayList<>();
    public static boolean flagPC = true;
    public int numIndex = 0;
    
    boolean firstLogin = true;
    
    PrintWriter outp = null;
    OutputStream out;
    Writer outWriter;
    BufferedWriter bufferWriter;
    InputStream in;
    InputStreamReader inReader;
    BufferedReader bufferedReader;
    OutputStream outVariavel;
    Writer outWriterVariavel;
    BufferedWriter bufferWriterVariavel;
    TelaServidor ts;
    Sala s;
    DefaultListModel model = new DefaultListModel();
    
     
    public Conexao (Socket aClientSocket, TelaServidor ts, Sala s) {
        try {
            //Recebe a instância tela servidor e sala
            this.ts = ts;
            this.s = s;
            //Recebe o socket do cliente
            clientSocket = aClientSocket;
            out = clientSocket.getOutputStream();
            outWriter = new OutputStreamWriter(out);
            bufferWriter = new BufferedWriter(outWriter);
            
            in = clientSocket.getInputStream();
            inReader = new InputStreamReader(in);
            bufferedReader = new BufferedReader(inReader);
            outp = new PrintWriter(clientSocket.getOutputStream(), true);
            atualizaOnline();
            this.start();
        } catch(IOException e){ 
            System.out.println("Connection:"+e.getMessage());
        }
        
    }
    
    public void limpaGSON(JSONGeral msg)
    {
        
        msg.setId(null);
        msg.setIp(null);
        msg.setNome(null);
        msg.setPorta(null);
        msg.setMsg(null);
        msg.setNomeenv(null);
        msg.setNomereceb(null);
        msg.setAcao(null);
        msg.setRes(null);
        msg.setEquipe(null);
        msg.setQuantidade(null);
        msg.setOp(null);
        
    }
    
    public void atualizaOnline(){
        try{
            
            model.clear();
            ts.lOnline.setModel(this.model);
            
           
            for (int i = 0; i < Conexao.listaClientes.size(); i++) {
                model.addElement(Conexao.listaClientes.get(i).getNOME() + " (" + (Conexao.listaClientes.get(i).getIP()) + ")" + " (" + Conexao.listaClientes.get(i).getPORTA() + ")"); 
                this.ts.lOnline.setModel(this.model);
                
            }

        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("");
        }catch (Exception e){
            System.out.println("");
        }
       
    }
    
    public void removerCliente(String nomeremov, int flag){
            try{
            
            
            for (int i = 0; i < listaClientes.size(); i++) {
                if(listaClientes.get(i).getNOME().equals(nomeremov)){
                   
                    this.ts.tEnviado.append("S. Removendo: " + listaClientes.get(i).getNOME() + "\n");
                    this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                    
               
                    listaClientes.remove(i);
                    listaSocket.remove(i);
                    socketp.remove(i);
                    clienteJson.remove(i);
                }
            }
 
            atualizaOnline();
     
        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("");
        }catch (Exception e){
            System.out.println("");
        }
    }
    //mudei coisas
    public synchronized void run(){

            Gson gson = new Gson();
            
            try {
                
                while(true){
                    BDCrud bd = new BDCrud();
                    String data = bufferedReader.readLine();
                    System.out.println(data);
                    
                    this.ts.tRecebido.append("S. recebido " + data + "\n");
                    this.ts.tRecebido.setCaretPosition(this.ts.tRecebido.getDocument().getLength());
                    JSONGeral msg = gson.fromJson(data, JSONGeral.class);

                    System.out.println(msg.getNome() + msg.getId());

                    if(msg == null || data == null){
                        
                        removerCliente(msg.getNome(), 0);
                        break;
                        
                    }
                    
                    if(firstLogin){
                        
                        enviaLista(data);
                        firstLogin = false;
                    }
                    
                    if(msg.getId().equals("4")){
                        if(msg.getOp().equals("0")){
                            
                            resul = bd.getLista(msg);
                            
                            int iteracao = 0;
                            while(iteracao < resul.size()){
                                
                                limpaGSON(msg);
                                msg.setNome(resul.get(iteracao).getNome());
                                msg.setEquipe(resul.get(iteracao).getEquipe());
                                msg.setQuantidade(resul.get(iteracao).getQuantidade());
                                msg.setOp("0");
                                msg.setId("4");
                                
                                String resultado = gson.toJson(msg);
                                this.ts.tEnviado.append("S. Enviado: " + resultado + "\n");
                                this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                                outp.println(resultado);
                                limpaGSON(msg);
                                ++iteracao;
                            }
                           
                            limpaGSON(msg);
                            msg.setAcao("4");
                            msg.setId("2");
                            msg.setRes("1");
                            
                            String enviar = gson.toJson(msg);
                            this.ts.tEnviado.append("S. Enviado: " + enviar + "\n");
                            this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                            outp.println(enviar);
                            limpaGSON(msg);
                            
                        }else if (msg.getOp().equals("1")){
                            
                            bd.adicionaRemedio(msg);
                            limpaGSON(msg);
                            msg.setAcao("4");
                            msg.setId("2");
                            msg.setRes("1");
                            
                            String enviar = gson.toJson(msg);
                            this.ts.tEnviado.append("S. Enviado: " + enviar + "\n");
                            this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                            outp.println(enviar);
                            limpaGSON(msg);
                            
                        }else if (msg.getOp().equals("2")){
                            
                            bd.remove(msg);
                            limpaGSON(msg);
                            msg.setAcao("4");
                            msg.setId("2");
                            msg.setRes("1");
                            
                            String enviar = gson.toJson(msg);
                            this.ts.tEnviado.append("S. Enviado: " + enviar + "\n");
                            this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                            outp.println(enviar);
                            limpaGSON(msg);
                            
                        }else if (msg.getOp().equals("3")){
                            
                            bd.altera(msg);
                            limpaGSON(msg);
                            msg.setAcao("4");
                            msg.setId("2");
                            msg.setRes("1");
                            
                            String enviar = gson.toJson(msg);
                            this.ts.tEnviado.append("S. Enviado: " + enviar + "\n");
                            this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                            outp.println(enviar);
                            limpaGSON(msg);
                            
                        } 
                    }
                    
                    else if(msg.getId().equals("1")){
                        
                        listaSocket.add(this.clientSocket);
                        socketp.add(outp);
                        String ip = clientSocket.getInetAddress().toString();
                        Clientes cliente;
                        cliente = new Clientes(msg.getNome(), ip, Integer.toString(clientSocket.getPort()));
                        listaClientes.add(cliente);
                        clienteJson.add(data);
                        
                        
                        limpaGSON(msg);
                        msg.setId("2");
                        msg.setAcao("1");
                        msg.setRes("1");
                        String json = gson.toJson(msg);
                        this.ts.tEnviado.append("S. Enviado: " + json + "\n");
                        this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                        outp.println(json);
                        atualizaOnline();
                        limpaGSON(msg);

                        
                        for (int i = 0; i < listaClientes.size(); i++) {
                            outp.println(clienteJson.get(i));
                            System.out.println("send do buffer writer");
                        }
                        
                    }else if(msg.getId().equals("0")){
                        String nomeremov = msg.getNome();
                        limpaGSON(msg);
                        msg.setId("2");
                        msg.setAcao("0");
                        msg.setRes("1");
                        String json = gson.toJson(msg);
                        outp.println(json);
                        removerCliente(nomeremov, 0);
                        envioLogout(data);
                        limpaGSON(msg);
                        
                    }else if(msg.getId().equals("3")){
                        
                        int indice = encontraDestinatario(msg);
                        if(indice != -1){
                            
                            PrintWriter chat = socketp.get(indice);
                            chat.println(data);
                            
                        }
                    }
                    atualizaOnline();
                }
            } catch(EOFException e){
                System.out.println("EOF:"+e.getMessage());
                this.ts.tRecebido.append("EOF:"+e.getMessage() + "\n");
                this.ts.tRecebido.setCaretPosition(this.ts.tRecebido.getDocument().getLength());
            } catch(IOException e){
                this.ts.tRecebido.append("S. Cliente Desconectado. \n");
                this.ts.tRecebido.setCaretPosition(this.ts.tRecebido.getDocument().getLength());
                
            }catch(ArrayIndexOutOfBoundsException ex){
                System.out.println("");
            }catch (Exception e){
                System.out.println("");
            }
        
    }
    
    private void envioLogout(String data){
    
        Enumeration e =  Collections.enumeration(socketp);
                    while (e.hasMoreElements()){
                        
                        this.ts.tEnviado.append("S. Enviado: " + data + "\n");
                        this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
                        
                        PrintWriter chat = (PrintWriter) e.nextElement();
                        
                        chat.println(data);

                    }
        
    }
    
    private void enviaLista(String json) throws IOException {
       // try{
            this.ts.tEnviado.append("Broadcast: " + json + "\n");
            this.ts.tEnviado.setCaretPosition(this.ts.tEnviado.getDocument().getLength());
        
            for (int i = 0; i < listaSocket.size(); i++) {
                PrintWriter envio = (PrintWriter) socketp.get(i);
                envio.println(json);
            }

    } 
    
    
    public int encontraDestinatario(JSONGeral msg) {
        for (int i = 0; i < listaClientes.size(); i++) {
            
            if(listaClientes.get(i).getNOME().equals(msg.getNomereceb())){
                return i;
            }
        }
        return -1;
    }
}
