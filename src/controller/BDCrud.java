/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import com.google.gson.Gson;
import java.sql.*;
import java.util.*;;
import javax.swing.JOptionPane;

/**
 *
 * @author nickv
 */
public class BDCrud {
    
    /* ----CONEXÃO COM O BD-> */
    private Connection conexao;
    private Gson gson;
    
    // Estabelece uma conexão
    public BDCrud() throws SQLException {       
        this.conexao = BDCriaConexao.getConexao();
    }
    /* <-CONEXÃO COM O BD---- */
    
    /* ----REMEDIO-> */
    
    // CREATE - Adiciona um registro
    public void adicionaRemedio(JSONGeral c) throws SQLException {
        // Prepara conexão p/ receber o comando SQL
        String sql = "INSERT INTO REMEDIO(nome, equipe, quantidade)" + "VALUES(?, ?, ?)";    
        PreparedStatement stmt;
 
        // stmt recebe o comando SQL
        stmt = this.conexao.prepareStatement(sql);

        // Seta os valores p/ o stmt, substituindo os "?"
        stmt.setString(1,c.getNome());
        stmt.setString(2, c.getEquipe());
        stmt.setInt(3, Integer.parseInt(c.getQuantidade()));
        
        // O stmt executa o comando SQL no BD, e fecha a conexão
        stmt.executeUpdate();
        JOptionPane.showMessageDialog(null,"Adicionado com sucesso!");
        stmt.close();
            
    }
    
    // SELECT - Retorna uma lista com o resultado da consulta
    public List<JSONGeral> getLista(JSONGeral c) throws SQLException{
        // Prepara conexão p/ receber o comando SQL
        String sql = "SELECT * FROM remedio WHERE nome like ?";
        PreparedStatement stmt = this.conexao.prepareStatement(sql);
        stmt.setString(1, c.getNome());
        
        // Recebe o resultado da consulta SQL
        ResultSet rs = stmt.executeQuery();

        List<JSONGeral> lista = new ArrayList<>();
        
        // Enquanto existir registros, pega os valores do ReultSet e vai adicionando na lista
        while(rs.next()) {
            //  A cada loop, é instanciado um novo objeto, p/ servir de ponte no envio de registros p/ a lista
            JSONGeral d = new JSONGeral();
            
            // "d" -> remedio novo - .setNome recebe o campo do banco de String "nome" 
            d.setNome(rs.getString("nome"));
            d.setEquipe(rs.getString("equipe"));
            d.setQuantidade(rs.getString("quantidade"));
         
            // Adiciona o registro na lista
            lista.add(d); 
            //System.out.println(c.getNome() + c.getEquipe());
        }
   
        // Fecha a conexão com o BD
        rs.close();
        stmt.close();

        // Retorna a lista de registros, gerados pela consulta
        return lista;   
        
    }
       
    // UPDATE - Atualiza registros
    public void altera(JSONGeral c) throws SQLException {
        // Prepara conexão p/ receber o comando SQL
        String sql = "UPDATE remedio set nome=?, equipe=?, quantidade=?" + "WHERE nome=?";
        // stmt recebe o comando SQL
        PreparedStatement stmt = this.conexao.prepareStatement(sql);
        
        // Seta os valores p/ o stmt, substituindo os "?"
        stmt.setString(1, c.getNome());
        stmt.setString(2, c.getEquipe());
        stmt.setInt(3, Integer.parseInt(c.getQuantidade()));
        stmt.setString(4, c.getNome());
        
        // O stmt executa o comando SQL no BD, e fecha a conexão
        stmt.execute();
        JOptionPane.showMessageDialog(null,"Alterado com sucesso!");
        stmt.close();
        
    }
    
    // DELETE - Apaga registros
    public void remove(JSONGeral c) throws SQLException {       
        // Prepara conexão p/ receber o comando SQL
        String sql = "DELETE FROM remedio WHERE nome=?";
        // stmt recebe o comando SQL
        PreparedStatement stmt = this.conexao.prepareStatement(sql);
        
        // Seta o valor do nome p/ a condição de verificação SQL, dentro do stmt
        stmt.setString(1, c.getNome());
        
        // Executa o codigo SQL, e fecha
        stmt.execute();
        JOptionPane.showMessageDialog(null,"Removido com sucesso!");
        stmt.close();
        
    }
    /* <-REMEDIO---- */
}
