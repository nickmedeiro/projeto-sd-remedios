package controller;

import View.Sala;
import java.io.*;
import java.net.*;
import java.util.*;
import com.google.gson.Gson;
import static controller.Conexao.resul;
import javax.swing.*;

public class Cliente extends Thread{   
    
    private Socket ClientSocket = null;
    private Gson gson = new Gson();
    private JSONGeral jg = new JSONGeral();
    private JSONRes res = new JSONRes();
    String nome;
    String porta;
    PrintWriter outp = null;
    OutputStream out;
    Writer outWriter;
    BufferedWriter bufferWriter;
    InputStream in;
    InputStreamReader inReader;
    BufferedReader bufferedReader;
    Sala sala;
    public ArrayList<Clientes> listaClientes = new ArrayList<Clientes>();
    public ArrayList<Clientes> listaClientesProntos = new ArrayList<Clientes>();
    Clientes client;
    DefaultListModel model = null;
    DefaultListModel modelList = new DefaultListModel();
    DefaultListModel listaremedio = new DefaultListModel();
    public int numSorteado;
    
    /**
     *
     * @param nome
     * @param equipe
     * @param quantidade
     * @throws java.io.IOException
     */
    public void enviaRemedioBD(String nome, String equipe, String quantidade) throws IOException  {
        
        JSONGeral json = new JSONGeral();
        
        json.setNome(nome);
        json.setEquipe(equipe);
        json.setQuantidade(quantidade);
        json.setId("4");
        json.setOp("1");
        
        String enviar = gson.toJson(json);
        
        outp.flush();
        outp.println(enviar);
        limpaGSON(json);

    }
    
    public void pesquisaRemedioBD(String nome) throws IOException{
    
        JSONGeral json = new JSONGeral();
        
        json.setNome(nome);
        json.setEquipe("");
        json.setQuantidade(null);
        json.setId("4");
        json.setOp("0");
       
        String enviar = this.gson.toJson(json);
        
        outp.flush();
        outp.println(enviar);
        limpaGSON(json);

    }
    
    public void atualizaRemedioBD(String nome, String equipe, String quantidade) throws IOException{
    
        JSONGeral json = new JSONGeral();
        
        json.setNome(nome);
        json.setEquipe(equipe);
        json.setQuantidade(quantidade);
        json.setId("4");
        json.setOp("3");
        
        String enviar = this.gson.toJson(json);
        
        outp.flush();
        outp.println(enviar);
        limpaGSON(json);
    
    }
    
    public void excluiClienteBD (String nome) throws IOException{
    
        JSONGeral json = new JSONGeral();
        
        json.setNome(nome);
        json.setEquipe(null);
        json.setQuantidade(null);
        json.setId("4");
        json.setOp("2");
        
        String enviar = this.gson.toJson(json);
        
        outp.flush();
        outp.println(enviar);
        limpaGSON(json);

    }
    
    public void conectarCliente(String serverIP, int serverPort, String portString, String nome, Sala sala) throws IOException 
    {
        try {
            this.sala = sala;
            this.ClientSocket = new Socket(serverIP, serverPort);
            outp = new PrintWriter(ClientSocket.getOutputStream(), true);
            
            out = ClientSocket.getOutputStream();
            outWriter = new OutputStreamWriter(out);
            bufferWriter = new BufferedWriter(outWriter);
            
            in = ClientSocket.getInputStream();
            inReader = new InputStreamReader(in);
            bufferedReader = new BufferedReader(inReader);
            
            //codigo novo
            this.jg.setNome(nome);
            this.nome = nome;
            this.jg.setId("1");
            this.jg.setPorta(portString);
            this.porta = portString;
            this.jg.setIp("127.0.0.1");
            //fim
          
            String json = this.gson.toJson(jg);
            System.out.println("Enviado: " + json);
            
            outp.println(json);
            //codigo novo
            String received = bufferedReader.readLine();
            System.out.println(received);
            this.res = gson.fromJson(received, JSONRes.class);
            //fim
            this.sala.tMensagem.append(this.nome.toUpperCase() + " entrou. \n");
            this.sala.tMensagem.setCaretPosition(this.sala.tMensagem.getDocument().getLength());
            atualizaUsuarios();
            this.start();

        } catch (UnknownHostException e) {
            System.err.println("Host desconhecido: ");
            System.exit(1);
        } catch (IOException e) {
            System.err.println("IP ou Porta não existe ");
            System.exit(1);
        } catch (Exception e) {
            System.err.println("Falha na conexão com o servidor");
        }
    }
    
    public int checagemJSON(JSONGeral jg){
    
        switch(jg.getId()){
            case "0":
                return 0;
            case "1":
                return 1;
            case "2":
                return 2;
            case "3":
                return 3;
        }
        
        return -1;
    }
    
    public void limpaGSON(JSONGeral msg)
    {
        
        msg.setId(null);
        msg.setIp(null);
        msg.setNome(null);
        msg.setPorta(null);
        msg.setMsg(null);
        msg.setNomeenv(null);
        msg.setNomereceb(null);
        msg.setAcao(null);
        msg.setRes(null);
        msg.setEquipe(null);
        msg.setQuantidade(null);

    }
   //mudei coisas aqui
    public void enviarChat(String msgText) throws IOException{
        
            this.limpaGSON(jg);
            this.jg.setId("3");
            this.jg.setNomeenv(this.nome);
            
            System.out.println(msgText);
            int selecionado = sala.lOnline.getSelectedIndex();
            Clientes nomer = this.listaClientes.get(selecionado);
            System.out.println(nomer.getNOME());
            this.jg.setNomereceb(nomer.getNOME());
            System.out.println(jg.getNomereceb());
            this.jg.setMsg(msgText);
            
            if (selecionado == -1) {
                
            } else {
                
                this.sala.tMensagem.append("Você disse: " + msgText + "\n");
                this.sala.tMensagem.setCaretPosition(this.sala.tMensagem.getDocument().getLength());
                
                String json = this.gson.toJson(this.jg);
                System.out.println("C. Enviado: " + json);
                outp.println(json);
                sala.lOnline.clearSelection();
            }
     
    }
    
     
    public void logout() throws IOException{
            
            limpaGSON(jg);
            this.jg.setId("0");
            this.jg.setNome(this.nome);
            this.jg.setIp("127.0.0.1");
            this.jg.setPorta(porta);
            String json = this.gson.toJson(jg);
            System.out.println("enviando " + json);
            outp.println(json);
            
    }
    
    public synchronized void run(){
        try {    
            while (true) {
                
                    String received = bufferedReader.readLine();
                    System.out.println("C. Recebido: " + received);
                    JSONGeral msgServidor = gson.fromJson(received, JSONGeral.class);
                   
                    if (msgServidor.getId().equals("4")){
                        if(msgServidor.getOp().equals("0")){
                        
                            try{
                            
                                //listaremedio.clear();
                                sala.jListRemedios.setModel(this.listaremedio);

                                listaremedio.addElement("(" + msgServidor.getNome() + ")" + " (" + msgServidor.getEquipe() + ")" + " (" + msgServidor.getQuantidade() + ")"); 
                                this.sala.jListRemedios.setModel(this.listaremedio);
                          
                            }catch(ArrayIndexOutOfBoundsException ex){
                                    System.out.println("");
                            }catch (Exception e){
                                    System.out.println("");
                            }
                        
                            
                        } 

                    }
                    
                    else if(msgServidor.getId().equals("1"))
                    {
                       
                        client = new Clientes(msgServidor.getNome(), msgServidor.getIp(), msgServidor.getPorta());
                        listaClientes.add(client);
                        
                        atualizaUsuarios();
                         
                    }else if(msgServidor.getId().equals("0") && msgServidor.getNome().equals(nome)){
                        
                        client = new Clientes(msgServidor.getNome(), msgServidor.getIp(), msgServidor.getPorta());
                        listaClientes.remove(client);
                        atualizaUsuarios();
                        break;
                        
                    }else if(msgServidor.getId().equals("0")){
                        
                        for(int i = 0; i<listaClientes.size(); i++){
                        
                            if(msgServidor.getNome().equals(listaClientes.get(i).getNOME())){
                                listaClientes.remove(i);
                            }
                        }

                        atualizaUsuarios();
                        
                    }else if(msgServidor.getId().equals("3")){ 
                       
                            this.sala.tMensagem.append(msgServidor.getNomeenv() + " disse: " + msgServidor.getMsg() + "\n");
                            this.sala.tMensagem.setCaretPosition(this.sala.tMensagem.getDocument().getLength());

                    }else if(msgServidor.getId().equals("2")){
                        
                        atualizaUsuarios();
                        System.out.println(msgServidor.getId() + " res " + msgServidor.getRes() + " acao " + msgServidor.getAcao());
                    }
                    
                atualizaUsuarios();
                
            }
            this.in.close();
            this.out.close();
            this.ClientSocket.close();
            
        }catch (IOException ex) {
            System.out.println("Erro:" + ex);
        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("--------------");
        }catch (Exception e){
            System.out.println("Erro:" + e);
        }
              
    }
    
    public int verificaUsuarioLista(Clientes c){
        if(this.listaClientes != null && this.listaClientes.size() > 0){
            Iterator<Clientes> itClientes = listaClientes.iterator();
            while(itClientes.hasNext()){
                Clientes cliente = itClientes.next();
                if(cliente.getIP().equals(c.getIP()) && cliente.getPORTA().equals(c.getPORTA())){
                    return 1;
                }
            }
            
        }
        return 0;
    }
    
    
    public synchronized void atualizaUsuarios(){
        try{
            model = new DefaultListModel();
            this.model.clear();
            sala.lOnline.setModel(this.model);
            JList list = new JList(model);

            int estaNaLista = 0;
            
            Iterator<Clientes> it = listaClientes.iterator(); 
            while (it.hasNext()) { 
                Clientes c = it.next();
                
                int online = verificaUsuarioLista(c);

                if(online == 1){
                    
                    model.addElement(c.getNOME() + " (" + (c.getIP()) + ") (" + c.getPORTA() + ")"); 
                    this.sala.lOnline.setModel(model);
                   
                }else{
                    
                        System.out.println("n/a");
                }

            }
        }catch(ArrayIndexOutOfBoundsException ex){
            System.out.println("");
        }catch (Exception e){
            System.out.println("");
        }
               
    }

}
